-- * header
-------------------------------------------------------------------------------
-- Title      : vedic multiplier test bench
-- Project    :
-------------------------------------------------------------------------------
-- File       : vedic_multiplier_tb.vhd
-- Author     : Jose Correcher <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-12-14
-- Last update: 2021-09-01
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This core creates a test bench for vedic multiplier core
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-12-14  1.0      jcorrecher  Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

library osvvm;
context osvvm.osvvmcontext;

use work.osvvm_pkg.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity vedic_multiplier_tb is

  generic (
    G_SEED      : string  := "none";
    G_CROSS     : boolean := false;
    G_DATA_TYPE : boolean := true;
    G_DATA_W    : natural := 8;
    G_FAILS     : natural := 10;
    G_DLY       : natural := 8;
    G_SCENARIO  : string  := "scenario_1";
    G_TSIM      : natural := 1000);

end entity vedic_multiplier_tb;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture sim of vedic_multiplier_tb is

-- ** constant declaration

  -- *** vedic delay
  constant C_VEDIC_DLY : natural := 4;
  -- *** output widthç
  constant C_DATAOUT_W : natural := 2 * G_DATA_W;
  -- *** clock period definition
  constant C_CLK_P     : time    := 2.000 ns;

-- ** type declaration
  type T_MULT is array (0 to C_VEDIC_DLY-1) of signed(C_DATAOUT_W-1 downto 0);

-- ** signal declaration
  signal tbc_clk_s    : std_logic := '0';
  signal tbc_rst_s    : std_logic := '1';
  signal tbc_ena_s    : std_logic := '0';
  --
  signal tb0_driver_rc : rc_osvvm_bus(vedic(C_DATAOUT_W-1 downto 0),
                                      mult(C_DATAOUT_W-1 downto 0),
                                      data_a(G_DATA_W-1 downto 0),
                                      data_b(G_DATA_W-1 downto 0));
  signal tb0_ena_r    : std_logic                      := '0';
  signal tb0_a_data_r : signed(G_DATA_W-1 downto 0)    := (others => '0');
  signal tb0_b_data_r : signed(G_DATA_W-1 downto 0)    := (others => '0');
  signal tb0_fail_r   : std_logic                      := '0';
  --
  signal tb1_valid_s  : std_logic                      := '0';
  signal tb1_data_s   : signed(C_DATAOUT_W-1 downto 0) := (others => '0');
  --
  signal tb2_a_data_s : signed(G_DATA_W-1 downto 0)    := (others => '0');
  signal tb2_b_data_s : signed(G_DATA_W-1 downto 0)    := (others => '0');
  signal tb2_data_r   : signed(C_DATAOUT_W-1 downto 0) := (others => '0');
  signal tb2_ena_r    : std_logic                      := '0';
  --
  signal tb3_monitor_rc : rc_osvvm_bus(vedic(C_DATAOUT_W-1 downto 0),
                                       mult(C_DATAOUT_W-1 downto 0),
                                       data_a(G_DATA_W-1 downto 0),
                                       data_b(G_DATA_W-1 downto 0));

begin

-------------------------------------------------------------------------------
-- * TBA : Asserts
-------------------------------------------------------------------------------

  assert not (G_DATA_W >= 32) report "OSVVM doesn't allow range greater than 32" severity failure;

  assert not (G_CROSS = true and G_DATA_W > 8) report
    "Enable Cross Coverage with higher widths degrades the performance simulation" severity failure;

-------------------------------------------------------------------------------
-- * TBC : Control signals
-------------------------------------------------------------------------------

  --! clock generator
  tbc_clk_s    <= not tbc_clk_s after C_CLK_P/2;
  --! reset generator
  tbc_rst_s    <= '0'           after C_CLK_P*100;
  --! generate tb enable
  tbc_ena_s    <= '1'           after C_CLK_P*120;

-------------------------------------------------------------------------------
-- * TB0 : OSVVM driver
-------------------------------------------------------------------------------

-- ** driver instance

  driver : entity work.osvvm_driver
    generic map (
      G_SEED      => G_SEED,
      G_CROSS     => G_CROSS,
      G_DATA_TYPE => G_DATA_TYPE,
      G_DATA_W    => G_DATA_W,
      G_FAILS     => G_FAILS,
      G_DLY       => G_DLY)
    port map (
      driver_bus => tb0_driver_rc);

-- ** assignment

-- *** inputs
  tb0_driver_rc.clk    <= tbc_clk_s;
  tb0_driver_rc.rst    <= tbc_rst_s;
  tb0_driver_rc.enasim <= tbc_ena_s;

-- *** outputs

  tb0_a_data_r <= tb0_driver_rc.data_a;
  tb0_b_data_r <= tb0_driver_rc.data_b;
  tb0_ena_r    <= tb0_driver_rc.data_valid;
  tb0_fail_r   <= tb0_driver_rc.force_failure;

-------------------------------------------------------------------------------
-- * TB1 : DUT Vedic multiplier
-------------------------------------------------------------------------------

  dut : entity work.vedic_multiplier
    port map (
      clk       => tbc_clk_s,
      ic_ena    => tb0_ena_r,
      id_a      => tb0_a_data_r,
      id_b      => tb0_b_data_r,
      oc_valid  => tb1_valid_s,
      od_result => tb1_data_s);

-------------------------------------------------------------------------------
-- * TB2 : Functional multiplier
-------------------------------------------------------------------------------

  -- force failures
  tb2_a_data_s <= tb0_a_data_r when tb0_fail_r = '0' else not (tb0_a_data_r) + 1;
  tb2_b_data_s <= tb0_b_data_r;

  functional_mult : process
  begin

    wait until rising_edge(tbc_ena_s);

    while tbc_ena_s loop
      wait until rising_edge(tbc_clk_s);
      tb2_data_r <= tb2_a_data_s * tb2_b_data_s;
      tb2_ena_r  <= tb0_ena_r;
    end loop;
    wait;
  end process functional_mult;

-------------------------------------------------------------------------------
-- * TB3 : OSVVM Monitor
-------------------------------------------------------------------------------

-- ** monitor instance

  monitor : entity work.osvvm_monitor
    generic map (
      G_OPER_DLY  => C_VEDIC_DLY,
      G_CLK_P     => C_CLK_P,
      G_CROSS     => G_CROSS,
      G_DATA_TYPE => G_DATA_TYPE,
      G_DATA_W    => G_DATA_W,
      G_FAILS     => G_FAILS,
      G_SCENARIO  => G_SCENARIO,
      G_TSIM      => G_TSIM)
    port map (
      monitor_bus => tb3_monitor_rc);

-- ** assignment

  tb3_monitor_rc.clk         <= tbc_clk_s;
  tb3_monitor_rc.rst         <= tbc_rst_s;
  tb3_monitor_rc.enasim      <= tbc_ena_s;
  tb3_monitor_rc.fail_done   <= tb0_driver_rc.fail_done;
  tb3_monitor_rc.data_valid  <= tb0_ena_r;
  tb3_monitor_rc.data_a      <= tb0_a_data_r;
  tb3_monitor_rc.data_b      <= tb0_b_data_r;
  tb3_monitor_rc.vedic_valid <= tb1_valid_s;
  tb3_monitor_rc.vedic       <= tb1_data_s;
  tb3_monitor_rc.mult_valid  <= tb2_ena_r;
  tb3_monitor_rc.mult        <= tb2_data_r;

end architecture sim;
