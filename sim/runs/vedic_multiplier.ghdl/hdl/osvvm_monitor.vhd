-- * header
-------------------------------------------------------------------------------
-- Title      : vedic multiplier simulation
-- Project    :
-------------------------------------------------------------------------------
-- File       : osvvm_monitor.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2021-06-11
-- Last update: 2021-09-01
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This entity monitors input stimulus, vedic and model outputs
-- and generate some verification metrics.
-------------------------------------------------------------------------------
-- Copyright (c) 2021
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2021-06-11  1.0      jcorrecher  Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.ScoreBoardPkg_slv.all;
use osvvm.CoveragePkg.all;
context osvvm.osvvmcontext;

use work.osvvm_pkg.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity osvvm_monitor is

  generic
    (G_OPER_DLY  : natural := 4;
     G_CLK_P     : time    := 2 ns;
     G_CROSS     : boolean := false;
     G_DATA_TYPE : boolean := true;
     G_DATA_W    : natural := 8;
     G_FAILS     : natural := 10;
     G_SCENARIO  : string  := "scenario_1";
     G_TSIM      : natural := 1000);

  port (
    monitor_bus : inout rc_osvvm_bus);

end entity osvvm_monitor;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------
architecture model of osvvm_monitor is

-- ** alias declaration

  -- *** inputs
  alias clk         : std_logic is monitor_bus.clk;
  alias rst         : std_logic is monitor_bus.rst;
  alias enasim      : std_logic is monitor_bus.enasim;
  alias fail_done   : boolean is monitor_bus.fail_done;
  alias data_a      : signed is monitor_bus.data_a;
  alias data_b      : signed is monitor_bus.data_b;
  alias data_valid  : std_logic is monitor_bus.data_valid;
  alias vedic_valid : std_logic is monitor_bus.vedic_valid;
  alias vedic       : signed is monitor_bus.vedic;
  alias mult_valid  : std_logic is monitor_bus.mult_valid;
  alias mult        : signed is monitor_bus.mult;

-- ** constant declaration
  constant C_ENDSIM : time :=  G_TSIM * 1 us;

-- ** signal declaration

  shared variable tb0_sb_sv : ScoreBoardPType;
  --
  signal tb1_a_done_s       : boolean         := false;
  signal tb1_b_done_s       : boolean         := false;
  --
  signal tb2_sign_done_s    : boolean         := false;
  signal tb2_cross_done_s   : boolean         := false;
  --
  signal tb4_sboard_done_s : std_logic := '0';
  signal tb4_test_done_s   : integer_barrier := 1;

begin

-------------------------------------------------------------------------------
-- * TB0 : ScoreBoard
-------------------------------------------------------------------------------

-- ** get simulated data
  --! the simulated data is stored when clock rising edge is reached and vedic
  --! multiplier output valid is asserted
  sb_gen : process
  begin

    --! creating log
    SetAlertLogName("Vedic Scoreboard");
    tb0_sb_sv.SetAlertLogId(Name => "vedic_scoreboard");

    wait until falling_edge(rst);
    while not tb4_sboard_done_s loop
      wait until falling_edge(clk) and mult_valid = '1';
      tb0_sb_sv.Push(std_logic_vector(mult));
    end loop;
    wait;

  end process sb_gen;

-- ** get expected data
  --! the expected data is stored and checked when clock falling edge is
  --! reached and vedic multiplier output valid is asserted
  sb_check : process
  begin

    wait until falling_edge(rst);
    while not tb4_sboard_done_s loop
      wait until falling_edge(clk) and vedic_valid = '1';
      if (not tb0_sb_sv.empty) then
        tb0_sb_sv.Check(std_logic_vector(vedic));
      end if;
    end loop;
    ReportAlerts;
    wait;

  end process sb_check;

-------------------------------------------------------------------------------
-- * TB1 : Functional Coverage
-------------------------------------------------------------------------------

  --! create functional coverage for input A
  osvvm_fcoverage(G_DATA_TYPE, G_DATA_W, 0, G_SCENARIO,
                  clk, rst, data_valid, data_a, tb1_a_done_s);
  --! create functional coverage for input B
  osvvm_fcoverage(G_DATA_TYPE, G_DATA_W, 1, G_SCENARIO,
                  clk, rst, data_valid, data_b, tb1_b_done_s);

-------------------------------------------------------------------------------
-- * TB2 : Functional Cross Coverage
-------------------------------------------------------------------------------

  --! create functional sign cross coverage
  osvvm_fcoverage_sign(G_DATA_W, G_SCENARIO,
                       clk, enasim, data_valid,
                       data_a(data_a'high),data_b(data_b'high), tb2_sign_done_s);

  --! create functional data cross coverage
  fcc : if G_CROSS = true generate
    osvvm_fcoverage_cross(G_DATA_W, G_SCENARIO,
                          clk, data_valid, data_a, data_b, tb2_cross_done_s);
  end generate fcc;

-------------------------------------------------------------------------------
-- * TB3 : Test Done
-------------------------------------------------------------------------------

  test_done : process
  begin

    --! when G_CROSS is true, the test must be finished when input cross data
    --!is reached, else when sign cross, input a and b are reached
    if G_CROSS = true then
      wait until tb2_cross_done_s;
      wait for G_OPER_DLY * G_CLK_P;
      tb4_sboard_done_s <= '1';
      tb4_test_done_s <= 0;
    else
      if G_FAILS /= 0 then
        wait until fail_done;
        wait for G_OPER_DLY * G_CLK_P;
        tb4_sboard_done_s <= '1';
        tb4_test_done_s <= 0;
      else
        wait until tb2_sign_done_s and tb1_a_done_s and tb1_b_done_s;
        wait for G_OPER_DLY * G_CLK_P;
        tb4_sboard_done_s <= '1';
        tb4_test_done_s <= 0;
      end if;
    end if;
    wait;
  end process test_done;

-------------------------------------------------------------------------------
-- * TB4 : Report summary
-------------------------------------------------------------------------------

  rep_mon : process
    variable v_log : AlertLogIDType;
  begin

    --! wait for test has been done
    WaitForBarrier(tb4_test_done_s, C_ENDSIM);
    AlertIf(now >= C_ENDSIM, "Test finished due to timeout");
    wait until rising_edge(clk);
    --! final report
    BlankLine(1);
    v_log := GetAlertLogID("Final Report");
    SetLogEnable(v_log, INFO, TRUE);
    Log(v_log, "Input A Data Covered: " & to_string(tb1_a_done_s), INFO);
    Log(v_log, "Input B Data Covered: " & to_string(tb1_b_done_s), INFO);
    Log(v_log, "Sign Cross Covered: " & to_string(tb2_sign_done_s), INFO);
    Log(v_log, "Input Data Cross Covered: " & to_string(tb2_cross_done_s), INFO);
    BlankLine(1);
    std.env.stop;
    wait;
  end process rep_mon;

end architecture model;
