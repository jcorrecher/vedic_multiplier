-- * header
-------------------------------------------------------------------------------
-- Title      : vedic multiplier osvvm
-- Project    :
-------------------------------------------------------------------------------
-- File       : osvvm_multiplier.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2021-06-11
-- Last update: 2021-09-01
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This package contains specific procedures used to evaluate
-- vedic multiplier core
-------------------------------------------------------------------------------
-- Copyright (c) 2021
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2021-06-11  1.0      jcorrecher  Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional libraries
-------------------------------------------------------------------------------

use work.function_pkg.all;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.ScoreBoardPkg_slv.all;
use osvvm.CoveragePkg.all;
context osvvm.osvvmcontext;

-------------------------------------------------------------------------------
-- * package declaration
-------------------------------------------------------------------------------

package osvvm_pkg is

  type rc_osvvm_bus is record
    clk           : std_logic;
    rst           : std_logic;
    enasim        : std_logic;
    force_failure : std_logic;
    fail_done     : boolean;
    data_valid    : std_logic;
    data_a        : signed;
    data_b        : signed;
    vedic_valid   : std_logic;
    vedic         : signed;
    mult_valid    : std_logic;
    mult          : signed;
  end record rc_osvvm_bus;

  procedure osvvm_gen_input_rand (
    constant G_DATA_W : in  natural;
    constant G_SEED   : in  string;
    constant G_CROSS  : in  boolean;
    constant G_DLY    : in  natural;
    signal clk        : in  std_logic;
    signal rst        : in  std_logic;
    signal ena        : in  std_logic;
    signal valid      : out std_logic;
    signal data_a     : out signed;
    signal data_b     : out signed);

  procedure osvvm_gen_input_incr (
    constant G_DATA_W : in  natural;
    constant G_DLY    : in  natural;
    signal clk        : in  std_logic;
    signal rst        : in  std_logic;
    signal ena        : in  std_logic;
    signal valid      : out std_logic;
    signal data_a     : out signed;
    signal data_b     : out signed);

  procedure osvvm_gen_fails (
    constant G_FAILS : in  natural;
    constant G_SEED  : in  string;
    signal clk       : in  std_logic;
    signal rst       : in  std_logic;
    signal valid     : in  std_logic;
    signal fail      : out std_logic;
    signal done      : out boolean);

  procedure osvvm_fcoverage (
    constant G_DATA_TYPE : in  boolean;
    constant G_DATA_W    : in  natural;
    constant G_POS       : in  natural;
    constant G_SCENARIO  : in  string;
    signal clk           : in  std_logic;
    signal rst           : in  std_logic;
    signal valid         : in  std_logic;
    signal data          : in  signed;
    signal done          : out boolean);

  procedure osvvm_fcoverage_cross (
    constant G_DATA_W   : in  natural;
    constant G_SCENARIO : in  string;
    signal clk          : in  std_logic;
    signal valid        : in  std_logic;
    signal data_a       : in  signed;
    signal data_b       : in  signed;
    signal done         : out boolean);

  procedure osvvm_fcoverage_sign (
    constant G_DATA_W   : in  natural;
    constant G_SCENARIO : in  string;
    signal clk          : in  std_logic;
    signal ena          : in  std_logic;
    signal valid        : in  std_logic;
    signal sign_a       : in  std_logic;
    signal sign_b       : in  std_logic;
    signal done         : out boolean);

end package osvvm_pkg;

-------------------------------------------------------------------------------
-- * package body
-------------------------------------------------------------------------------

package body osvvm_pkg is

-------------------------------------------------------------------------------
-- * random input data procedure body
-------------------------------------------------------------------------------
--! when G_DATA_TYPE is false, A & B will be random values based on G_SEED seed.
--! if G_CROSS is true, intelligent coverage is enabled to cover all the
--! cross posibilities randomly.

  procedure osvvm_gen_input_rand (
    constant G_DATA_W : in  natural;
    constant G_SEED   : in  string;
    constant G_CROSS  : in  boolean;
    constant G_DLY    : in  natural;
    signal clk        : in  std_logic;
    signal rst        : in  std_logic;
    signal ena        : in  std_logic;
    signal valid      : out std_logic;
    signal data_a     : out signed;
    signal data_b     : out signed
    ) is

    variable v_log           : AlertLogIDType;
    variable v_a_rand        : RandomPType;
    variable v_b_rand        : RandomPType;
    variable v_cross         : CovPType;
    variable v_a_msb_rand    : RandomPType;
    variable v_a_lsb_rand    : RandomPType;
    variable v_b_msb_rand    : RandomPType;
    variable v_b_lsb_rand    : RandomPType;
    variable v_crosswalk_msb : integer := 0;
    variable v_crosswalk_lsb : integer := 0;
    variable v_cnt           : natural := 0;
    variable v_data_a        : integer := 0;
    variable v_data_b        : integer := 0;
  begin

    --! set log alert
    BlankLine(1);
    v_log := GetAlertLogID("Data generation");
    SetLogEnable(v_log, INFO, TRUE);

    if G_DATA_W > 8 then
      Log(v_log, "Random data enabled", INFO);
      BlankLine(1);
      --! initializing values
      wait until (rising_edge(clk) and rst = '1');
      v_a_msb_rand.InitSeed(G_SEED(1 to G_SEED'length/2-1));
      v_a_lsb_rand.InitSeed(G_SEED(G_SEED'length/2 to G_SEED'length-1));
      v_b_msb_rand.InitSeed(G_SEED(G_SEED'length/2 to G_SEED'length-1));
      v_b_lsb_rand.InitSeed(G_SEED(1 to G_SEED'length/2-1));
      v_crosswalk_lsb := to_integer(f_crosswalk(G_DATA_W/2, true));
      v_crosswalk_msb := to_integer(f_crosswalk(G_DATA_W/2, false));
      v_cnt           := 0;
      valid           <= '0';
      data_a          <= (others => '0');
      data_b          <= (others => '0');
      wait until rising_edge(ena);
      while ena loop
        wait until rising_edge(clk);
        --! update data when enable asserts
        if v_cnt = G_DLY then
          v_cnt := 0;
          valid <= '1';
          --! assign random values
          data_a <= signed(v_a_msb_rand.Randslv((0, (2**G_DATA_W/2)-1, v_crosswalk_lsb, v_crosswalk_msb),
                                                G_DATA_W/2) &
                           v_a_lsb_rand.Randslv((0, (2**G_DATA_W/2)-1, v_crosswalk_lsb, v_crosswalk_msb),
                                                G_DATA_W/2));
          data_b <= signed(v_b_msb_rand.Randslv((0, (2**G_DATA_W/2)-1, v_crosswalk_lsb, v_crosswalk_msb),
                                                G_DATA_W/2) &
                           v_b_lsb_rand.Randslv((0, (2**G_DATA_W/2)-1, v_crosswalk_lsb, v_crosswalk_msb),
                                                G_DATA_W/2));
        else
          v_cnt := v_cnt + 1;
          valid <= '0';
        end if;
      end loop;

    else

      -- select random data
      if G_CROSS = true then
        Log(v_log, "Intelligent coverage enabled", INFO);
        --! generate bins
        v_cross.AddCross(GenBin(0, (2**G_DATA_W)-1), GenBin(0, (2**G_DATA_W)-1));
      else
        Log(v_log, "Random data enabled", INFO);
      end if;

      BlankLine(1);
      --! initializing values
      wait until (rising_edge(clk) and rst = '1');
      v_a_rand.InitSeed(G_SEED(G_SEED'length/2 to G_SEED'length-1));
      v_b_rand.InitSeed(G_SEED(1 to G_SEED'length/2-1));
      v_cnt  := 0;
      valid  <= '0';
      data_a <= (others => '0');
      data_b <= (others => '0');
      wait until rising_edge(ena);
      while ena loop
        wait until rising_edge(clk);
        --! update data when enable asserts
        if v_cnt = G_DLY then
          v_cnt := 0;
          valid <= '1';
          --! assign random value
          if G_CROSS = true then
            (v_data_a, v_data_b) := v_cross.RandCovPoint;
            data_a               <= to_signed(v_data_a, G_DATA_W);
            data_b               <= to_signed(v_data_b, G_DATA_W);
            v_cross.ICover((v_data_a, v_data_b));
          else
            data_a <= signed(v_a_rand.Randslv(0, 2**G_DATA_W-1, G_DATA_W));
            data_b <= signed(v_b_rand.Randslv(0, 2**G_DATA_W-1, G_DATA_W));
          end if;
        else
          v_cnt := v_cnt + 1;
          valid <= '0';
        end if;
      end loop;
    end if;
    wait;

  end procedure osvvm_gen_input_rand;

-------------------------------------------------------------------------------
-- * incremental input data procedure body
-------------------------------------------------------------------------------
--! when G_DATA_TYPE is true, A & B will be incremental values. when all A
--! possibilites are reached, B is increased.

  procedure osvvm_gen_input_incr (
    constant G_DATA_W : in  natural;
    constant G_DLY    : in  natural;
    signal clk        : in  std_logic;
    signal rst        : in  std_logic;
    signal ena        : in  std_logic;
    signal valid      : out std_logic;
    signal data_a     : out signed;
    signal data_b     : out signed
    ) is

    variable v_log  : AlertLogIDType;
    variable v_data : unsigned(2*G_DATA_W-1 downto 0) := (others => '0');
    variable v_cnt  : natural                         := 0;
  begin

    --! set log alert
    BlankLine(1);
    v_log := GetAlertLogID("Data generation");
    SetLogEnable(v_log, INFO, TRUE);
    Log(v_log, "Incremental data enabled", INFO);
    BlankLine(1);

    --! initializing values
    wait until (rising_edge(clk) and rst = '1');
    v_data := (others => '0');
    valid  <= '0';
    data_a <= (others => '0');
    data_b <= (others => '0');
    wait until rising_edge(ena);

    while ena loop
      wait until rising_edge(clk);
      --! update data when enable asserts
      if v_cnt = G_DLY then
        v_cnt  := 0;
        valid  <= '1';
        --! assign incremental value
        v_data := v_data + 1;
        data_a <= signed(v_data(G_DATA_W-1 downto 0));
        data_b <= signed(v_data(2*G_DATA_W-1 downto G_DATA_W));
      else
        v_cnt := v_cnt + 1;
        valid <= '0';
      end if;
    end loop;
    wait;
  end procedure osvvm_gen_input_incr;

-------------------------------------------------------------------------------
-- * random failure for input data procedure body
-------------------------------------------------------------------------------
--! generate fails is a part of verification methodology. in a random time, a
--! control signal will be used to change one input data in multiplier model in
--! order to obtain a different value and generate a false error.

  procedure osvvm_gen_fails (
    constant G_FAILS : in  natural;
    constant G_SEED  : in  string;
    signal clk       : in  std_logic;
    signal rst       : in  std_logic;
    signal valid     : in  std_logic;
    signal fail      : out std_logic;
    signal done      : out boolean
    ) is
    variable v_log  : AlertLogIDType;
    variable v_rand : RandomPType;
  begin
    --! set log alert
    v_log := GetAlertLogID("Forced failure");
    SetLogEnable(v_log, INFO, TRUE);
    if G_FAILS > 0 then
      Log(v_log, "Enabled", INFO);
    else
      Log(v_log, "Disabled", INFO);
    end if;
    BlankLine(1);

    v_rand.InitSeed(G_SEED);
    fail <= '0';
    done <= false;

    for i in 1 to G_FAILS loop
      wait for v_rand.RandTime(10 * 1 us, 20 * 1 us);
      wait until rising_edge(clk) and valid = '1';
      fail <= '1';
      wait until rising_edge(clk) and valid = '1';
      fail <= '0';
      Log(v_log, "Forced error " & to_string(i) & " of " & to_string(G_FAILS), INFO);
    end loop;
    done <= true;
    wait;
  end procedure osvvm_gen_fails;

-------------------------------------------------------------------------------
-- * functional coverage procedure body
-------------------------------------------------------------------------------

  procedure osvvm_fcoverage (
    constant G_DATA_TYPE : in  boolean;
    constant G_DATA_W    : in  natural;
    constant G_POS       : in  natural;
    constant G_SCENARIO  : in  string;
    signal clk           : in  std_logic;
    signal rst           : in  std_logic;
    signal valid         : in  std_logic;
    signal data          : in  signed;
    signal done          : out boolean
    ) is
    variable v_log           : AlertLogIDType;
    variable v_data          : CovPType;
    variable v_crosswalk_msb : unsigned(G_DATA_W/2-1 downto 0) := (others => '0');
    variable v_crosswalk_lsb : unsigned(G_DATA_W/2-1 downto 0) := (others => '0');
    variable v_ones          : unsigned(G_DATA_W/2-1 downto 0) := (others => '1');
    variable v_zeros         : unsigned(G_DATA_W/2-1 downto 0) := (others => '0');

  begin

    --! Continue log
    v_log := GetAlertLogID("Functional Coverage");
    SetLogEnable(v_log, INFO, TRUE);

    if G_DATA_W > 8 then

      v_crosswalk_lsb := f_crosswalk(G_DATA_W/2, true);
      v_crosswalk_msb := f_crosswalk(G_DATA_W/2, false);

      v_data.AddBins(GenBin(0));
      v_data.AddBins(GenBin((2**G_DATA_W)-1));
      v_data.AddBins(GenBin(to_integer(v_ones & v_zeros)));
      v_data.AddBins(GenBin(to_integer(v_zeros & v_ones)));
      v_data.AddBins(GenBin(to_integer(v_crosswalk_lsb & v_zeros)));
      v_data.AddBins(GenBin(to_integer(v_zeros & v_crosswalk_lsb)));
      v_data.AddBins(GenBin(to_integer(v_crosswalk_lsb & v_ones)));
      v_data.AddBins(GenBin(to_integer(v_ones & v_crosswalk_lsb)));
      v_data.AddBins(GenBin(to_integer(v_crosswalk_msb & v_zeros)));
      v_data.AddBins(GenBin(to_integer(v_zeros & v_crosswalk_msb)));
      v_data.AddBins(GenBin(to_integer(v_crosswalk_msb & v_ones)));
      v_data.AddBins(GenBin(to_integer(v_ones & v_crosswalk_msb)));
      v_data.AddBins(GenBin(to_integer(v_crosswalk_msb & v_crosswalk_lsb)));
      v_data.AddBins(GenBin(to_integer(v_crosswalk_lsb & v_crosswalk_msb)));
      v_data.AddBins(GenBin(to_integer(v_crosswalk_lsb & v_crosswalk_lsb)));
      v_data.AddBins(GenBin(to_integer(v_crosswalk_msb & v_crosswalk_msb)));

    else
      --! Binding data for input B, 8bit = [0,255]
      v_data.AddBins(GenBin(0, (2**G_DATA_W)-1, 2**G_DATA_W));
    end if;
    --! Update at least from 1 to 10 || 100% x 10 = 1000
    v_data.SetCovTarget(1000.0);
    Log(v_log, "Data bin created for input " & to_string(G_POS), INFO);
    BlankLine(1);

    --! Analyzing input B until is covered
    while not v_data.IsCovered loop
      --! when data is available, adding data to cover
      wait until rising_edge(clk) and valid = '1';
      v_data.icover(to_integer(unsigned(data)));
    end loop;
    --! when coverage has done, show stats
    Log(v_log, "Input " & to_string(G_POS) & " data covered and stored at result folder", ALWAYS);
    BlankLine(1);
    v_data.WriteBin("result/" & G_SCENARIO & "/functional_in_" & to_string(G_POS) & "_coverage.txt", WRITE_MODE);
    done <= v_data.IsCovered;
    wait;

  end procedure osvvm_fcoverage;

-------------------------------------------------------------------------------
-- * functional cross coverage procedure body
-------------------------------------------------------------------------------
--! This procedure covers crossing data from two input data signals
--! when all the cross data possibilites are covered, results are stored in a
--! txt file at result folder

  procedure osvvm_fcoverage_cross (
    constant G_DATA_W   : in  natural;
    constant G_SCENARIO : in  string;
    signal clk          : in  std_logic;
    signal valid        : in  std_logic;
    signal data_a       : in  signed;
    signal data_b       : in  signed;
    signal done         : out boolean
    ) is
    variable v_log   : AlertLogIDType;
    variable v_cross : CovPType;
  begin
    --! Set Log
    v_log := GetAlertLogID("Functional Cross Coverage");
    SetLogEnable(v_log, INFO, TRUE);

    --! generate bins
    v_cross.AddCross(GenBin(0, (2**G_DATA_W)-1), GenBin(0, (2**G_DATA_W)-1));
    Log(v_log, "Cross data bin created", INFO);
    BlankLine(1);

    --! adding data to cover
    while not v_cross.IsCovered loop
      wait until rising_edge(clk) and valid = '1';
      v_cross.ICover((to_integer(unsigned(data_a)), to_integer(unsigned(data_b))));
    end loop;
    --! when coverage is done, save stats to file
    Log(v_log, "Data cross covered and stored at result folder", ALWAYS);
    BlankLine(1);
    v_cross.WriteBin("result/" & G_SCENARIO & "/functional_cross_coverage.txt", WRITE_MODE);
    done <= v_cross.IsCovered;
    wait;

  end procedure osvvm_fcoverage_cross;

-------------------------------------------------------------------------------
-- * functional sign coverage procedure body
-------------------------------------------------------------------------------

  procedure osvvm_fcoverage_sign (
    constant G_DATA_W   : in  natural;
    constant G_SCENARIO : in  string;
    signal clk          : in  std_logic;
    signal ena          : in  std_logic;
    signal valid        : in  std_logic;
    signal sign_a       : in  std_logic;
    signal sign_b       : in  std_logic;
    signal done         : out boolean
    ) is
    variable v_log  : AlertLogIDType;
    variable v_sign : CovPType;
  begin
    --! set log
    v_log := GetAlertLogID("Functional Cross Sign Coverage");
    SetLogEnable(v_log, INFO, TRUE);

    --! generate bins
    v_sign.AddCross(GenBin(0, 1), GenBin(0, 1));
    Log(v_log, "Cross sign bin created", INFO);
    BlankLine(1);
    wait until rising_edge(ena);

    --! adding sign to cover
    while not v_sign.IsCovered loop
      wait until rising_edge(clk) and valid = '1';
      v_sign.ICover((to_integer(unsigned'("" & sign_a)),
                     to_integer(unsigned'("" & sign_b))));
    end loop;
    --! when coverage is done, save stats to file
    Log(v_log, "Sign cross covered and stored at result folder", ALWAYS);
    BlankLine(1);
    v_sign.WriteBin("result/" & G_SCENARIO & "/functional_sign_cross_coverage.txt", WRITE_MODE);
    done <= v_sign.IsCovered;
    wait;

  end procedure osvvm_fcoverage_sign;

end package body osvvm_pkg;
