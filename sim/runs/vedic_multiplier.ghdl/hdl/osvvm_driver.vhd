-- * header
-------------------------------------------------------------------------------
-- Title      : vedic multiplier simulation
-- Project    :
-------------------------------------------------------------------------------
-- File       : osvvm_driver.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2021-06-11
-- Last update: 2021-09-01
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This entity provides input stimulus to evaluate vedic multiplier
-------------------------------------------------------------------------------
-- Copyright (c) 2021
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2021-06-11  1.0      jcorrecher  Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

library osvvm;
context osvvm.osvvmcontext;

use work.osvvm_pkg.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity osvvm_driver is

  generic (
    G_SEED      : string  := "none";
    G_CROSS     : boolean := false;
    G_DATA_TYPE : boolean := true;
    G_DATA_W    : natural := 8;
    G_FAILS     : natural := 10;
    G_DLY       : natural := 8);

  port (
    driver_bus : inout rc_osvvm_bus);

end entity osvvm_driver;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------
architecture model of osvvm_driver is

-- ** alias declaration

  -- *** inputs
  alias clk    : std_logic is driver_bus.clk;
  alias rst    : std_logic is driver_bus.rst;
  alias enasim : std_logic is driver_bus.enasim;

  -- *** outputs
  alias data_a     : signed is driver_bus.data_a;
  alias data_b     : signed is driver_bus.data_b;
  alias data_valid : std_logic is driver_bus.data_valid;
  alias fail       : std_logic is driver_bus.force_failure;
  alias fail_done  : boolean is driver_bus.fail_done;

begin


-------------------------------------------------------------------------------
-- * TB0 : Incremental input data
-------------------------------------------------------------------------------

  t_incr : if G_DATA_TYPE = true generate
    osvvm_gen_input_incr(G_DATA_W, G_DLY, clk, rst, enasim, data_valid, data_a, data_b);
  end generate t_incr;

-------------------------------------------------------------------------------
-- * TB1 : Random input data
-------------------------------------------------------------------------------

  t_rand : if G_DATA_TYPE = false generate

    osvvm_gen_input_rand(G_DATA_W, G_SEED, G_CROSS, G_DLY,
                         clk, rst, enasim, data_valid, data_a, data_b);

  end generate t_rand;

-------------------------------------------------------------------------------
-- * TB2 : Generate random failure enable
-------------------------------------------------------------------------------

  osvvm_gen_fails(G_FAILS, G_SEED, clk, rst, data_valid, fail, fail_done);


end architecture model;
