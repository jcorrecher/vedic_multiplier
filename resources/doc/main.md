\mainpage

\section Resume

This project contains a synthesizable design for a vedic multiplier based on VHDL code language.
[Vedic mathematics](https://en.wikipedia.org/wiki/Vedic_Mathematics) are based on Vedic Sutras, developed by Sri Bharti Krishna Tirathaji, can be applied to any branch of mathematics.
Its methods reduce complex calculations into simpler ones. The use of these methods produce a lower consume and a lower area occupancy of digital resources.

\subsection main_dig Digital design
The core multiplies two input data with the same width, which must be a power of two value. The result output width will be the sum of input widths. The internal processing deals with unsigned data, so input data must be detected and modified before and after data processing.

\subsection main_struct Documentation structure

The design documentation is structured as:

1. vedic_multiplier doxygen self-contained documentation.
2. [simulation scenarios.](simulation.md)
