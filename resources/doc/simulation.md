Different scenarios are deployed and grouped for input bit size in order to meet with verification plan.

\section scen_8bit Scenarios with 8bit input width

First group sets input data width to 8bit. Due to small data width, all the functionalities implemented in vedic multiplier, can be covered with all the possible cross inputs.

\subsection test_1 Test 1/5 : incremental data

First scenario sets input data as incremental, i.e. when all the possible A input data are crossed with the current B input data, B is increased. With this settings, all cross input data will be covered in almost 132 us.

Metrics covered in this scenario:
- [x] input width: 8bit.
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [x] all the possible cross input.
- [ ] decimated enable.
- [ ] failure detection.

Results:
- [code covered](../../scenario_1/syntax/index.html)
- [functional coverage for input 0](../../scenario_1/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_1/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_1/functional_sign_cross_coverage.txt)
- [functional cross coverage](../../scenario_1/functional_cross_coverage.txt)

\subsection test_2 Test 2/5 : random data

Second scenario sets input data as random values by using OSVVM random package. Seed is obtained from linux current date and splitted to feed both inputs. Random operations can delay timing simulation in order to cover the functionality. OSVVM itelligent coverage has been enabled to reduce time simulation.

Metrics covered with this scenario:
- [x] input width: 8bit.
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [x] all the possible cross input.
- [ ] decimated enable.
- [ ] failure detection.

Results:
- [code covered](../../scenario_2/syntax/index.html)
- [functional coverage for input 0](../../scenario_2/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_2/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_2/functional_sign_cross_coverage.txt)
- [functional cross coverage](../../scenario_2/functional_cross_coverage.txt)

\subsection test_3 Test 3/5 : failure forced

Third scenario is similarly to first scenario but sets a forced failure input
data to model multiplier. Verification plan must contains forced fails in order
to check error detector works properly.

Metrics covered with this scenario:
- [x] input width: 8bit.
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [x] all the possible cross input.
- [ ] decimated enable.
- [x] failure detection.

Results:
- [code covered](../../scenario_3/syntax/index.html)
- [functional coverage for input 0](../../scenario_3/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_3/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_3/functional_sign_cross_coverage.txt)
- [functional cross coverage](../../scenario_3/functional_cross_coverage.txt)

\subsection test_4 Test 4/5 : enable decimated

Fourth scenario sets a decimated enable, in order to refresh a value each n
clock cycles (where n is the number of clock cycles without enable). Input data
is set as random and intelligent coverage. For a decimated enable value set as 4,
simulation time is increased for at least 4x132us.

Metrics covered with this
scenario:
- [x] input width: 8bit.
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [x] all the possible cross input.
- [x] decimated enable.
- [ ] failure detection.

Results:
- [code covered](../../scenario_4/syntax/index.html)
- [functional coverage for input 0](../../scenario_4/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_4/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_4/functional_sign_cross_coverage.txt)
- [functional cross coverage](../../scenario_4/functional_cross_coverage.txt)

\subsection test_5 Test 5/5 : enable and failures

Fifth scenario is a mix of third and fourth scenarios. This last scenario
covers all the targets.

Metrics covered with this scenario:
- [x] input width: 8bit.
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [x] all the possible cross input.
- [x] decimated enable.
- [x] failure detection.

Results:
- [code covered](../../scenario_5/syntax/index.html)
- [functional coverage for input 0](../../scenario_5/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_5/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_5/functional_sign_cross_coverage.txt)
- [functional cross coverage](../../scenario_5/functional_cross_coverage.txt)

\section scen_16bit Scenarios with 16bit input width

Second group sets input data to 16bit. To cover all the possible cross input data, 4294967296 operations must be executed. Hence is necessary at least 8.59 seconds simulation time to cover all the input cross operations. Jobs handled by the shared runners on GitLab time out after 3 hours. To validate verification plan, specific set of values will be driven as random data. This set of random data ensures that all the processing paths will be exercised.

The following diagram shows a processing example:<br>
<img src="../images/vedic_example.png" width="600px" />

\subsection test_6 Test 1/4 : random data

First scenario sets input data as random values by using OSVVM random package.
Seed is obtained from linux current date and splitted to feed both inputs.
Random operations can delay timing simulation in order to cover the
functionality. A reduced set of input data have been driven.

Metrics covered with this scenario:
- [x] input width: 16bit
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [ ] all the possible cross input.
- [ ] decimated enable.
- [ ] failure detection.

Results:
- [code covered](../../scenario_6/syntax/index.html)
- [functional coverage for input 0](../../scenario_6/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_6/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_6/functional_sign_cross_coverage.txt)

\subsection test_7 Test 2/4 : failure forced

Second scenario is similarly to first scenario but sets a forced failure input
data to model multiplier. Verification plan must contains forced fails in order
to verify errors can be reproduced.

Metrics covered with this scenario:
- [x] input width: 16bit
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [ ] all the possible cross input.
- [ ] decimated enable.
- [x] failure detection.

Results:
- [code covered](../../scenario_7/syntax/index.html)
- [functional coverage for input 0](../../scenario_7/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_7/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_7/functional_sign_cross_coverage.txt)

\subsection test_8 Test 3/4 : enable decimated

Third scenario sets a decimated enable, in order to refresh a value each n clock
cycles (where n is the number of clock cycles without enable). Input data is set
as random and intelligen coverage. For a decimated enable value set as 4,
simulation time is increased for at least x4 times.

Metrics covered with this scenario:
- [x] input width: 16bit
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [ ] all the possible cross input.
- [x] decimated enable.
- [ ] failure detection.

Results:
- [code covered](../../scenario_8/syntax/index.html)
- [functional coverage for input 0](../../scenario_8/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_8/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_8/functional_sign_cross_coverage.txt)

\subsection test_9 Test 4/4 : enable and failures

Fourth scenario sets a decimated enable, in order to refresh a value each n clock
cycles (where n is the number of clock cycles without enable). Input data is set
as random and intelligen coverage. For a decimated enable value set as 4,
simulation time is increased for at least x4 times. Injected errors.

Metrics covered with this scenario:
- [x] input width: 16bit
- [x] exercise sign detection and correction.
- [x] all the possible values for inputs independently.
- [ ] all the possible cross input.
- [x] decimated enable.
- [x] failure detection.

Results:
- [code covered](../../scenario_9/syntax/index.html)
- [functional coverage for input 0](../../scenario_9/functional_in_0_coverage.txt)
- [functional coverage for input 1](../../scenario_9/functional_in_1_coverage.txt)
- [functional sign cross coverage](../../scenario_9/functional_sign_cross_coverage.txt)
