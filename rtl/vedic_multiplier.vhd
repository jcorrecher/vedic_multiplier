-- * header
-------------------------------------------------------------------------------
-- Title      : Vedic Multiplier
-- Project    :
-------------------------------------------------------------------------------
--! \file     vedic_multiplier.vhd
--! \author   Jose Correcher <jose.correcher@gmail.com>
--! \date     Created    : 2020-12-03
--!           Last update: 2021-04-13
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description:
--! \class    vedic_multiplier
--! \details
--!           This core contains a model of a generic multiplier based on vedic
--!           mathematics.
--! \todo     include generic to avoid sign detection.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-12-03  1.0      jcorrecher  Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.function_pkg.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity vedic_multiplier is

  port (
    --!\name clock
    --!\{
    clk       : in  std_logic;
    --!\}
    --!\name enable
    --!\{
    ic_ena    : in  std_logic;
    --!\}
    --!\name input data
    --!\{
    id_a      : in  signed;
    id_b      : in  signed;
    --!\}
    --!\name output valid
    --!\{
    oc_valid  : out std_logic;
    --!\}
    --!\name output result
    --!\{
    od_result : out signed);
  --!\}

end entity vedic_multiplier;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of vedic_multiplier is

-------------------------------------------------------------------------------
--!\class          vedic_multiplier::rtl
--!\subpage
--!                This is the architecture for a vedic multiplier for two
--!                input signed data. <br>
--!                <img src="../images/vedic_full.png" width="800px">

-- ** constant declaration

--!\name find max input length
--!\{
  constant C_DATA_W : natural := f_mlen(id_a'length,id_b'length);
--!\}
--!\name internal width constants
--!\{
  constant C_SPLIT_W : natural := natural(ceil(real(C_DATA_W) / 2.0));
  constant C_MULT_W  : natural := 2 * C_SPLIT_W;
  constant C_SUM1_W  : natural := C_MULT_W + 1;
  constant C_SUM2_W  : natural := C_SUM1_W + 1;
  constant C_SUM3_W  : natural := C_SUM2_W + 1;
--!\}
--!\name divide internal data into
--!\{
  constant C_NUM_SPLIT : natural := 2;
--!\}
--!\name total processing delay
--!\{
  constant C_SIGN_DLY : natural := 4;
  constant C_ENA_DLY  : natural := 5;
--!\}

-- ** type declaration
  type T_SPLIT is array (0 to C_NUM_SPLIT-1) of unsigned(C_SPLIT_W-1 downto 0);

-- ** signal declaration

--!\name input assignment and sign detection
--!\{
  signal bi_a_r         : unsigned(id_a'range)           := (others => '0');
  signal bi_b_r         : unsigned(id_a'range)           := (others => '0');
  signal bi_sign_r      : std_logic                      := '0';
  signal bi_a_s         : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
  signal bi_b_s         : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
--!\}
--!\name split data
--!\{
  signal b0_a_sa        : T_SPLIT                        := (others => (others => '0'));
  signal b0_b_sa        : T_SPLIT                        := (others => (others => '0'));
--!\}
--!\name first stage
--!\{
  signal b1_mult1_r     : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
  signal b1_mult2_r     : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
  signal b1_mult3_r     : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
  signal b1_mult4_r     : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
--!\}
--!\name second stage
--!\{
  signal b2_sum_r       : unsigned(C_SUM1_W-1 downto 0)  := (others => '0');
  signal b2_mult1_r     : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
  signal b2_mult4_r     : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
--!\}
--!\name third stage
--!\{
  signal b3_sum_r       : unsigned(C_SUM2_W-1 downto 0)  := (others => '0');
  signal b3_mult1_msb_s : unsigned(C_SUM2_W-1 downto 0)  := (others => '0');
  signal b3_mult1_lsb_r : unsigned(C_SPLIT_W-1 downto 0) := (others => '0');
  signal b3_mult4_r     : unsigned(C_MULT_W-1 downto 0)  := (others => '0');
--!\}
--!\name fourth stage
--!\{
  signal b4_sum_r       : unsigned(C_SUM3_W-1 downto 0)  := (others => '0');
  signal b4_sum_msb_s   : unsigned(C_SUM3_W-1 downto 0)  := (others => '0');
  signal b4_mult1_lsb_r : unsigned(C_SPLIT_W-1 downto 0) := (others => '0');
  signal b4_sum_lower_r : unsigned(C_SPLIT_W-1 downto 0) := (others => '0');
--!\}
--!\name sign correction
--!\{
  signal b5_ena_ra      : unsigned(0 to C_ENA_DLY-1)     := (others => '0');
  signal b5_sign_ra     : unsigned(0 to C_SIGN_DLY-1)    := (others => '0');
--!\}
--!\name output data resize
--!\{
  signal b6_data_s      : unsigned(od_result'range)      := (others => '0');
--!\}

begin

-------------------------------------------------------------------------------
-- * BA : Asserts
-------------------------------------------------------------------------------

  -- ** input data must have the same length

--!\class    vedic_multiplier::rtl
--!\warning  A and B input data must have the same length

  assert id_a'length = id_b'length
    report "ERROR - input data must have the same length" severity failure;

  -- ** input data length must be even

--!\class    vedic_multiplier::rtl
--!\warning  The input data length must be even

  assert C_DATA_W mod 2 = 0 report "ERROR - input data length must be even" severity failure;

-------------------------------------------------------------------------------
-- * BI : Data input sign detection
-------------------------------------------------------------------------------
--!\class       vedic_multiplier::rtl
--!\subsection  vedic_isign  Input data sign detection
--! Input data must be processed as unsigned data, so input sign must be
--! detected and corrected at the end of the process.

  --! sign detector for A and B inputs
  sign_detect: process (clk) is
  begin
    if rising_edge(clk) then
      -- input data a
      if (id_a(id_a'high) = '1') then
        bi_a_r <= unsigned(not (id_a) + 1);
      else
        bi_a_r <= unsigned(id_a);
      end if;
      -- input data b
      if (id_b(id_b'high) = '1') then
        bi_b_r <= unsigned(not (id_b) + 1);
      else
        bi_b_r <= unsigned(id_b);
      end if;
      -- sign detector
      bi_sign_r <= std_logic(id_a(id_a'high) xor id_b(id_b'high));
    end if;
  end process sign_detect;

  --!\class  vedic_multiplier::rtl
  --! Then resize data if data input length are odd.
  bi_a_s <= resize(bi_a_r, C_MULT_W);
  bi_b_s <= resize(bi_b_r, C_MULT_W);

-------------------------------------------------------------------------------
-- * B0 : Split input data
-------------------------------------------------------------------------------
--!\class       vedic_multiplier::rtl
--!\subsection  vedic_split  Split the input data
--! The input data is splitted into LSB and MSB portions.

  u_split : for i in 0 to C_NUM_SPLIT-1 generate
    b0_a_sa(i) <= bi_a_s((i+1)*C_SPLIT_W-1 downto i*C_SPLIT_W);
    b0_b_sa(i) <= bi_b_s((i+1)*C_SPLIT_W-1 downto i*C_SPLIT_W);
  end generate u_split;

-------------------------------------------------------------------------------
-- * B1 : Multiply
-------------------------------------------------------------------------------
--!\class       vedic_multiplier::rtl
--!\subsection  vedic_s1  Stage 1
--! In the first stage LSB and MSB portions of input data are multiplied.

  --! First multipier stage
  stage1: process (clk) is
  begin
    if rising_edge(clk) then
      b1_mult1_r <= b0_a_sa(0) * b0_b_sa(0);
      b1_mult2_r <= b0_a_sa(0) * b0_b_sa(1);
      b1_mult3_r <= b0_a_sa(1) * b0_b_sa(0);
      b1_mult4_r <= b0_a_sa(1) * b0_b_sa(1);
    end if;
  end process stage1;

-------------------------------------------------------------------------------
-- * B2 : middle adder
-------------------------------------------------------------------------------
--!\class       vedic_multiplier::rtl
--!\subsection  vedic_s2  Stage 2
--! The second stage adds second and third multiplier in order to calculate middle data

  --! Second stage with add and delays
  stage2: process (clk) is
  begin
    if rising_edge(clk) then
      -- add operation
      b2_sum_r   <= resize(b1_mult2_r, b2_sum_r'length) + resize(b1_mult3_r, b2_sum_r'length);
      -- shift register
      b2_mult1_r <= b1_mult1_r;
      b2_mult4_r <= b1_mult4_r;
    end if;
  end process stage2;

-------------------------------------------------------------------------------
-- * B3 : lower adder
-------------------------------------------------------------------------------
--!\class       vedic_multiplier::rtl
--!\subsection  vedic_s3  Stage 3
--! In the third stage adds middle adder and MSB of first multiplier resize
--! to middle adder size

  --! third stage with add and delays
  stage3: process (clk) is
  begin
    if rising_edge(clk) then
      -- add operation
      b3_sum_r       <= b3_mult1_msb_s + resize(b2_sum_r, b3_sum_r'length);
      -- shift register
      b3_mult1_lsb_r <= b2_mult1_r(C_SPLIT_W-1 downto 0);
      b3_mult4_r     <= b2_mult4_r;
    end if;
  end process stage3;

  -- select MSB part of first multiplier and resize to lower adder size
  b3_mult1_msb_s <= resize(b2_mult1_r(b2_mult1_r'length-1 downto C_SPLIT_W), b3_sum_r'length);

-------------------------------------------------------------------------------
-- * B4 : higher adder
-------------------------------------------------------------------------------
--!\class       vedic_multiplier::rtl
--!\subsection  vedic_s4  Stage 4
--! A final processing stage where fourth multiplier and MSB of middle are added
--! then adder resize to fourth multiplier size

  --! fourth stage with add and delays
  stage4: process (clk) is
  begin
    if rising_edge(clk) then
      -- add operation
      b4_sum_r       <= b4_sum_msb_s + resize(b3_mult4_r, b4_sum_r'length);
      -- shift register
      b4_mult1_lsb_r <= b3_mult1_lsb_r(C_SPLIT_W-1 downto 0);
      b4_sum_lower_r <= b3_sum_r(C_SPLIT_W-1 downto 0);
    end if;
  end process stage4;

  -- select MSB part of lower adder and resize to higher adder size
  b4_sum_msb_s <= resize(b3_sum_r(b3_sum_r'length-1 downto C_SPLIT_W), b4_sum_r'length);

-------------------------------------------------------------------------------
-- * B5 : enable shift register
-------------------------------------------------------------------------------
--!\class       vedic_multiplier::rtl
--!\subsection  vedic_srl  Control Delays
--! This block shifts enable and control sign to synchronize with result data.

  --! delays enable and sign detection
  srl_ctrl: process (clk) is
  begin
    if rising_edge(clk) then
      b5_ena_ra  <= ic_ena & b5_ena_ra(0 to b5_ena_ra'length-2);
      b5_sign_ra <= bi_sign_r & b5_sign_ra(0 to b5_sign_ra'length-2);
    end if;
  end process srl_ctrl;

-------------------------------------------------------------------------------
-- * B6 : compose output data
-------------------------------------------------------------------------------
--!\class       vedic_multiplier::rtl
--!\subsection  vedic_out  Output data composer
--! Finally output data is composed with:
--! - LSB of first multiplier
--! - LSB of lower adder
--! - higher adder

  b6_data_s <= b4_sum_r(C_MULT_W-1 downto 0) & b4_sum_lower_r & b4_mult1_lsb_r;
  oc_valid  <= b5_ena_ra(b5_ena_ra'high);
  od_result <= signed(b6_data_s) when b5_sign_ra(b5_sign_ra'high) = '0' else signed(not(b6_data_s) + 1);

end architecture rtl;
