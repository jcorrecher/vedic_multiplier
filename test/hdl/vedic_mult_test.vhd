-- * header
-------------------------------------------------------------------------------
-- Title      : Vedic Multiplier test
-- Project    :
-------------------------------------------------------------------------------
-- File       : top.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-07-28
-- Last update: 2021-01-12
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This file wraps Vedic Multiplier with registered signals to
--              validate as synthesizable design
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-07-28  1.0      jcorrecher Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity vedic_mult_test is

  generic (
    G_DATA_W : natural := 16);

  port (
    clk       : in  std_logic;
    ic_ena    : in  std_logic;
    id_a      : in  signed(G_DATA_W-1 downto 0);
    id_b      : in  signed(G_DATA_W-1 downto 0);
    oc_valid  : out std_logic;
    od_result : out signed(2*G_DATA_W-1 downto 0));

end entity vedic_mult_test;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture test of vedic_mult_test is

-- ** signal declaration
  signal b0_ena_r   : std_logic               := '0';
  signal b0_a_r     : signed(id_a'range)      := (others => '0');
  signal b0_b_r     : signed(id_b'range)      := (others => '0');
  --
  signal b1_full_s  : std_logic               := '0';
  signal b1_valid_s : std_logic               := '0';
  signal b1_data_s  : signed(od_result'range) := (others => '0');

begin

-------------------------------------------------------------------------------
-- * B0 : write clock domain
-------------------------------------------------------------------------------

  process (clk) is
  begin
    if rising_edge(clk) then
      b0_ena_r <= ic_ena;
      b0_a_r   <= id_a;
      b0_b_r   <= id_b;
    end if;
  end process;

-------------------------------------------------------------------------------
-- * B1 : component instance
-------------------------------------------------------------------------------

  i_multiplier : entity work.vedic_multiplier
    port map (
      clk       => clk,
      ic_ena    => b0_ena_r,
      id_a      => b0_a_r,
      id_b      => b0_b_r,
      oc_valid  => b1_valid_s,
      od_result => b1_data_s);
-------------------------------------------------------------------------------
-- * B2 : output register
-------------------------------------------------------------------------------

  process (clk) is
  begin
    if rising_edge(clk) then
      oc_valid  <= b1_valid_s;
      od_result <= b1_data_s;
    end if;
  end process;

end architecture test;
