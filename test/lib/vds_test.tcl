# set variables
set design_name $env(design_name)
set fpga_device $env(fpga_device)
set num_cores   $env(num_cores)

# Create report folder
if { ![file exists "./rpt/"] } {
    file mkdir ./rpt
}

# Create project folder
if { ![file exists "./prj/"] } {
    file mkdir ./prj
}

cd ./prj

# create project
create_project $design_name -force

# set project properties
set_property "target_language" "VHDL" [current_project]
set_property "default_lib" "xil_defaultlib" [current_project]
set_property part $fpga_device [get_project $design_name]

# add files
add_files -fileset [get_filesets constrs_1] "../lib/${design_name}.xdc"
add_files {../../rtl ../../common/hdl ../hdl}

# set top file and compile order
set_property "top" $design_name [get_filesets sources_1]
update_compile_order -fileset [get_filesets sources_1]
#remove unnecesary files
remove_files [get_files -filter {IS_AUTO_DISABLED}]

# no iobuf placed by default
set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value {-mode out_of_context} -objects [get_runs synth_1]

# launch synthesis
if { [catch {[launch_runs synth_1 -jobs $num_cores]} emsg] } {
    puts "$emsg"
    puts "Synthesis Failed, please check errors\n"
}

wait_on_run synth_1
# save synthesis log in report folder
if { [file exists "./${design_name}.runs/synth_1/runme.log"] } {
    file copy -force "./${design_name}.runs/synth_1/runme.log" "../rpt/${design_name}_syn.rpt"
}

# launch implementation
if { [catch {[launch_runs impl_1 -jobs $num_cores]} emsg] } {
    puts "$emsg"
    puts "Implementation Failed, please check errors\n"
}

wait_on_run impl_1

# save implementation log in report folder
if { [file exists "./${design_name}.runs/impl_1/${design_name}.vdi"] } {
    file copy -force "./${design_name}.runs/impl_1/${design_name}.vdi" "../rpt/${design_name}_imp.rpt"
}

# open implemented design and save timing and utilization reports
open_run impl_1
report_utilization -file "../rpt/${design_name}_utilization.rpt"
report_timing_summary -warn_on_violation -file "../rpt/${design_name}_timing_summary.rpt"
report_timing -setup -file "../rpt/${design_name}_timing_setup.rpt"
report_timing -hold -file "../rpt/${design_name}_timing_hold.rpt"

# close project
close_project
cd ..
puts "Done\n"

exit
